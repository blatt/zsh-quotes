zsh_quotes=(

	## Doctor Who
	#Season 1
	"900 years of time and space, and I’ve never been slapped by someone’s mother.\n - The Doctor"
	"The Bene Gesserit witch must leave.\n - Guildsman"
	#Season 7
	"Do what I do. Hold tight and pretend it’s a plan!.\n - The Doctor"

  ## Rick and Morty
  "Nobody exists on purpose. Nobody belongs anywhere. We're all going to die. Come watch TV.\n - Morty"
  "I turned myself into a pickle. I'm Pickle Riiiiick.\n - Rick"

)
echo "${zsh_quotes[$(($RANDOM % ${#zsh_quotes[@]} + 1))]}"
echo
unset zsh_quotes
