## ZSH Quotes Plugin

Simple **zsh plugin** to print a random (film & book) quotes in terminal.

## Motivation
There are many zsh plugins for quotes. This is my version with quotes I like.

## Installation
TODO: Provide step by step series of examples and explanations about how to get a development env running.

## How to use?
TODO: If people like your project they’ll want to learn how they can use it. To do so include step by step guide to use your project.

## License

Distributed under the terms of the MIT License.

MIT © [Daniel Blatt](https://gitlab.com/blatt/zsh-quotes-plugin/raw/master/LICENSE)
